## 目标： 完成详情页的开发

当点击首页的热销推荐中的某一项，可以进入它对应的详情页。

1. 按照原来的思路，我们可以直接在li标签外面包裹一层`<router-link to="/detail"></router-link>`。 还是同样的问题，它会默认转换成一个a标签，里面的文字颜色就会被更改。在这里，我们换一种方式：直接将li变成router-link，然后再添加一个指示为li的tag属性，并动态的将页面跳转到`/detail/item.id`即：
```html
<ul>
    <router-link
        tag="li" 
        class="item border-bottom" 
        v-for="item of list" 
        :key="item.id"
        :to="'/detail/' + item.id"
    >
        <img class="item-img" :src="item.imgUrl" >
        <div class="item-info">
            <p class="item-title">{{item.title}}</p>
            <p class="item-desc">{{item.desc}}</p>
            <button class="item-button">查看详情</button>
        </div>
    </router-link>
</ul>
```

2. 添加路由配置：`router/index.js`添加一项动态路由项，即前面的路径必须是`/detail`，后面可以带一个参数，这个参数会放到变量id中

```javascript
 {
    path: '/detail/:id',
    name: 'Detail',
    component: Detail
}
```

3. 组件创建： `pages/detail/Detail.vue`
4. 完成基础模板搭建并引入到路由的index.js中
5. 开始进行详情页banner部分开发
   1. 创建组件并搭建基本框架：`detail/components/Banner.vue`
   2. 引入Detail组件并使用`<detail-banner></detail-banner>`
   3. 完成banner的布局与样式美化：
   ```html
   <div class="banner">
        <img class="banner-img" src="//img1.qunarzz.com/sight/p0/1707/82/821428e3bbf93bbaa3.water.jpg_600x330_2385695d.jpg" alt="">
        <div class="banner-info">
            <div class="banner-title">迪士尼小镇</div>
            <div class="banner-number">
                <span class="iconfont banner-icon">&#xe796;</span>
                39
            </div>
        </div>
    </div>
   ```

   ```html
   .banner
        overflow hidden
        height 0
        padding-bottom 55%
        position relative
        .banner-img
            width 100%
        .banner-info
            position absolute
            left 0
            right 0
            bottom 0
            display flex
            line-height .6rem
            color #fff
            .banner-title
                flex 1
                font-size .36rem
                padding 0 .2rem
            .banner-number
                padding 0 .2rem
                height .32rem
                line-height .32rem
                border-radius .2rem
                background rgba(0, 0, 0, .8)
                font-size .24rem
                margin-top .14rem 
                .banner-icon
                    font-size .24rem
   ```
   4. 添加图片的icon： 官网 ---> 找到图片 ---> 添加到项目 ---> 下载到本地 ---> 解压后需要两步操作
      1. 将项目中的`src/assets/styles/iconfont`下面的4个字体文件替换成download中的对应字体文件
      2. 将项目中的`src/assets/styles/iconfont.css`中的base64替换成download中的iconfont.css中的base64url
      3. 去官网复制对应的icon代码使用，download下来的文件就可以删除掉了
   5. banner底部一个线性渐变的效果
   给banner-infor添加一个样式：
  ```
  background-image linear-gradient(top, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.8))
  ``` 

6. 公用图片画廊组件拆分

当点击顶部banner的时候会弹出一个画廊组件，可以进行图片的轮播，下面会显示当前所在图片的页码，在项目中可能很多页面都会用到这样的功能效果，所以可以把它变成一个公用的组件：`src/common/gallery/Gallery.vue`。

在`build/webpack.base.conf.js`中添加一项别名：
```javascript
resolve: {
extensions: ['.js', '.vue', '.json'],
alias: {
    '@': resolve('src'),
    'styles': resolve('src/assets/styles'),
    'common': resolve('src/common'),
}
}
```
记得改完配置项后重启服务器。

在Banner.vue中使用Gallery.vue组件：`import CommonGallery from 'common/gallery/Gallery'`,绑定CommonGallery之后就可以使用`<common-gallery></common-gallery>`了。

有轮播的效果，这里也是使用swiper的那个插件，将首页中Swiper.vue的轮播部分借鉴过来，先将图片的位置固定，使其能够实现基本的轮播效果，我们希望在翻页的时候底部能够显示当前的图片页码，这里就需要设置swiper的option选项。[参考这里](https://3.swiper.com.cn/api/pagination/2016/0126/299.html)去找到对应的paginationType分页器样式类型。即：

布局：
```html
<div class="wrapper">
    <swiper :options="swiperOptions">
        <swiper-slide>
            <img class="gallery-img" src="http://img1.qunarzz.com/sight/p0/1707/c2/c2be8d6326cdea1a3.water.jpg_r_800x800_97e80887.jpg" />
        </swiper-slide>
        <swiper-slide>
            <img class="gallery-img" src="http://img1.qunarzz.com/sight/p0/1707/c4/c48e92049336b8ffa3.water.jpg_r_800x800_42a54b54.jpg" />
        </swiper-slide>
        <div class="swiper-pagination"  slot="pagination"></div>
    </swiper> 
</div>
```

样式：
```
.wrapper
    overflow hidden
    height 0
    width 100%
    padding-bottom 100%
    .gallery-img
        width 100%
    .swiper-pagination
        color #fff
```
分页器设置
```javascript
data () {
    return {
        swiperOptions: {
            pagination: '.swiper-pagination',
            paginationType: 'fraction'
        }
    }
}
```

此时会发现，分页器能够显示出来了，但是不是我们想要的位置，做一下调整：
首先将`.wrapper`的`overflow hidden`去掉，然后`.swiper-pagination`设置一个`bottom: -1rem`，然后会发现依然没有出现，是因为在`.swiper-container`也有一个`overflow hidden`限制了它的范围，所以设置：
```
.container >>> .swiper-container
        overflow inherit
```
至此，成功显示。

完成动态数据循环。图片中的src应该是动态变化的，就需要从外部传递一个数据过来,根据这个数据进行动态渲染：
```javascript
props: {
    imgs: {
        type: Array,
        default () {
            return ['http://img1.qunarzz.com/sight/p0/1707/c2/c2be8d6326cdea1a3.water.jpg_r_800x800_97e80887.jpg', 'http://img1.qunarzz.com/sight/p0/1707/c4/c48e92049336b8ffa3.water.jpg_r_800x800_42a54b54.jpg']
        }
    }
}
```
其中，type是指这个数据的类型，default是设置的默认值。然后就可以实现循环渲染：
```html
<swiper-slide v-for="(item, index) of imgs" :key="index">
    <img class="gallery-img" :src="item" />
</swiper-slide>
```

一般将default设置为空，从Banner中以属性的方式传递过来数据。所以需要设置Banner中的数据：
```
data() {
    return {
        imgs: ['http://img1.qunarzz.com/sight/p0/1707/c2/c2be8d6326cdea1a3.water.jpg_r_800x800_97e80887.jpg', 'http://img1.qunarzz.com/sight/p0/1707/c4/c48e92049336b8ffa3.water.jpg_r_800x800_42a54b54.jpg']
    }
}
```

进行一些逻辑上的实现。首先，这个图片的轮播希望能够控制它的显示和隐藏，不然那一开始的时候详情页的部分是会被遮挡的，所以，在Banner中使用这个组件的时候设置一个`v-show="showGallery"`来控制它的显示与隐藏效果。showGallery默认是false的，当我点击banner部分的时候希望它显示出来，所以给banner绑定一个点击事件:
```javascript
methods: {
    handleBannerClick () {
        this.showGallery = true
    }
}
```

此时，点击banner的时候会发现，轮播效果出现了问题，因为一开始`<common-gallery></common-gallery>`是处于一个隐藏的状态，当再把它显示出来的时候，swiper计算宽度会有问题，导致轮播图无法正常滚动。解决方法： 添加两个options选项：
```javascript
data () {
    return {
        swiperOptions: {
            pagination: '.swiper-pagination',
            paginationType: 'fraction',
            observeParents: true,
            observer: true
        }
    }
}
```
表示的是：当swiper这个插件监听到这个元素或者父元素DOM元素发生变化的时候会自动的自我刷新一次，通过这次自我更新就能解决宽度计算的问题。

当我点击gallery部分的时候希望它重新隐藏： 给container绑定一个点击事件`handleGalleryClick`，在Banner.vue中的`<common-gallery></common-gallery>`组件上监听这个事件，当监听到这个点击事件发生的时候就将`this.showGallery`设置为false。具体代码如下：

Gallery.vue:
```html
<div class="container" @click="handleGalleryClick">
    <div class="wrapper">
        <swiper :options="swiperOptions">
            <swiper-slide v-for="(item, index) of imgs" :key="index">
                <img class="gallery-img" :src="item" />
            </swiper-slide>
            <div class="swiper-pagination"  slot="pagination"></div>
        </swiper> 
    </div>
</div>
```

```javascript
methods: {
    handleGalleryClick () {
        this.$emit('close')
    }
}
```

Banner.vue:
```html
<common-gallery 
    :imgs="imgs" 
    v-show="showGallery"
    @close="handleBannerGallery"
></common-gallery>
```

```javascript
handleBannerGallery () {
    this.showGallery = false
}
```








