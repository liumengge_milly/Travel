## 目标： 页面搜索框

### 1. 准备工作：创建分支city-search,在该分支进行搜索框的开发
### 2. Search.vue组件的创建与导入
注意： 直接City.vue中使用`<city-search></city-search>`组件的时候，会报错，表示在template中必须要有一个外层的div将这些子组件包裹：
```html
<template>
    <div>
        <city-header></city-header>
        <city-search></city-search>
    </div>
</template>
```
### 3. 页面搜索框的布局
```html
<div class="search">
    <input class="search-input" type="text" placeholder="请输入城市或字母">
</div>
```
样式：
```html
.search
    height .72rem
    background $bgColor
    padding 0 .1rem
    .search-input
        width 100%
        height .62rem
        // line-height .62rem
        text-align center
        border-radius .06rem
        color #666
        padding 0 .1rem
        box-sizing border-box  // 设置padding的时候需要添加， 如果不添加，输入框就会被撑大
```

### 4. 代码提交