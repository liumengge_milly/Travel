## 目标: 开发首页轮播图

### 创建一个分支
码云上创建一个名为index-swiper的分支，然后在项目目录下`git pull`将在线的分支拉到本地，`git checkout index-swpier` 转到当前分支，首页轮播图的开发将在这个分支进行。

### 轮播插件应用：
1. [安装](https://github.com/surmon-china/vue-awesome-swiper)：
GitHub --->搜 vue-awesome-swiper --->  采用一个稍微老一点的稳定版本v2.6.7版本 ---> `npm install vue-awesome-swiper@2.6.7 --save` ---> 重启服务器 
2. 应用：
   1. 全局引入：在main.js中引入`import Vue from 'vue'和
import VueAwesomeSwiper from 'vue-awesome-swiper'`
   2. 引入css样式：main.js中`import 'swiper/dist/css/swiper.css'`
   3. 使用插件： 在main.js中使用`Vue.use(VueAwesomeSwiper, /* { default global options } */)` 
   
### 轮播图插件的初步使用
在components目录下创建Swiper.vue组件。
```html
<template>
  <swiper :options="swiperOption">
    <!-- slides -->
    <swiper-slide>I'm Slide 1</swiper-slide>
    <swiper-slide>I'm Slide 2</swiper-slide>
    <swiper-slide>I'm Slide 3</swiper-slide>
    <swiper-slide>I'm Slide 4</swiper-slide>
    <swiper-slide>I'm Slide 5</swiper-slide>
    <swiper-slide>I'm Slide 6</swiper-slide>
    <swiper-slide>I'm Slide 7</swiper-slide>
    <!-- Optional controls -->
    <div class="swiper-pagination"  slot="pagination"></div>
    <div class="swiper-button-prev" slot="button-prev"></div>
    <div class="swiper-button-next" slot="button-next"></div>
    <div class="swiper-scrollbar"   slot="scrollbar"></div>
  </swiper>
</template>

<script>
  export default {
    name: 'carrousel',
    data() {
      return {
        swiperOption: {
          // some swiper options/callbacks
          // 所有的参数同 swiper 官方 api 参数
          // ...
        }
      }
    }
  }
</script>

<style lang="stylus" scoped>

</style>
```
在Home组件中引入这个子组件：
`import HomeSwiper from './components/Swiper'`
即可只用`<home-swiper></home-swiper>`的组件了。

 
### Swiper.vue组件的开发

1. [图片资料获取](http://piao.qunar.com/touch/):直接找到想要的图片复制src的值即可。
2. 调整图片的宽高：`with:100%`
3. 会有抖动现象： 将Network的Online设置为3G网速，就会出现图片后面的内容抖动的问题，因为这个时候，图片是没有高度的。
4. 图片抖动的解决方法： 在`<swiper></swiper>`外层，套一个div，设置这个div的样式
    ```html
    .wrapper
        overflow: hidden
        width: 100%
        height: 0
        padding-bottom: 31.25%
    ```
    其中的31.25%指的是使用的图片的高与宽的比值。表示它的宽度是100%， 它的高度会相对于它的宽度撑开31.25%，为了保持宽高比例始终不变。
    或者：
    ```html
        width: 100%
        height: 31.25vw   <!--表示的是相当于31.25%的viewport的宽度，但是可能存在浏览器的兼容问题， 第一种是标准写法-->
    ```
5. 设置轮播图中的灰色○：
   1. 设置SwiperOption项 ：`pagination: '.swiper-pagination'`
   2. 样式设置：
   直接设置一个类样式是不行的：
   ```html
   .swiper-pagination-bullet-active
        background: #fff !import
   ```
    原因是：在这里设置的样式只修饰当前组件中的class，不会对其他组件产生影响，而swiper-pagination中显示的内容实际上是传递给swiper组件的，由swiper组件来决定在这里面显示什么样的内容。swiper显示出来的页码并不是我们这个组件显示的，而是在swiper这个组件中去做显示显示的。`.swiper-pagination-bullet-active` 这个class并不在当前的组件中，所以没有办法显示。
    正确写法：
    ```html
    <!--在style标签的最顶部-->
    .wrapper >>> .swiper-pagination-bullet-active
        background:  #fff
    ```
    上面的代码表示能够实现一个样式的穿透效果，该样式不受scoped的限制。

### 图片的循环呈现
在Swiper组件的数据对象中添加：
```javascript
swiperList: [{
    id: '0001',
    imgUrl: '图片地址'
},{
    id: '0002',
    imgUrl:  '图片地址'
}]
```
 实现轮播效果：
 ```html
 <swiper-slide v-for="item of swiperList" :key="item.id">
     <img class="swiper-img" :src="item.imgUrl"/>
 </swiper-slide>
 ```
循环轮播：
```html
SwiperOption: {
    pagination: '.swiper-pagination',
    loop: true
}
```
### 代码提交
```shell
1. 正常提交
git add .
git commit -m 'add swiper'
git push
2. 将该分支的代码合并到master分支
git checkout master
git merge origin/index-swiper  
git push
```
