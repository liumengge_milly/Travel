## 页面组件化
1. 在home目录下创建components目录。应用页面组件化的思想，将一个复杂的页面拆分成一部分一部分的内容，对于Home.vue来说，它对应的是整个页面最外层的一个容器组件，它会被拆分成很多很多小的组件，由这些小的组件帮助我们整合成最终的复杂页面。 把拆出来的一个个小的组件都放在components目录下，比如当前要进行的是header区域的开发， 就在components目录下创建Header.vue组件。
2. 在Home.vue中引入小组件：
   ```javascript
   import HomeHeader from './components/Header.vue'
    export default {
        name: 'Home',
        components: {
            // HomeHeader: HomeHeader    下面是ES6的写法
            HomeHeader,
        }
    }
   ```
3. 在Home组件中使用小组件
    ```javascript
    // 注意是小写，与其对应的组件名称的写法是不同的
    <home-header></home-header>
    ```
4. 此时，我们要做的header区域的开发，就是在做Header.vue组件的开发

## 使用css辅助开发工具stylus进行Header组件开发
stylus像less，sass一样，可以帮助我们在css中使用一些变量快速的编写css代码。
1. 安装
   ```shell
    npm install stylus --save
    npm install stylus-loader --save
   ```
2. 应用
```html
<style lang="stylus" scoped>
    .header 
    display: flex 
    line-height: .86rem
    background: #00bcd4
    color: #fff
    .header-left
        width: .64rem
        float: left
    .header-input
        flex: 1 
        height: .64rem 
        line-height: .64rem 
        margin-top: .12rem
        margin-left: .2rem 
        background: #ffffff
        border-radius: .1rem
        color: #ccc
    .header-right
        width: 1.24rem
        float: right 
        text-align: center
</style>
```
3. 几点说明：
   1. `<style lang="stylus" scoped></style>` 表示使用stylus的方式进行css样式的代码编写，要遵循stylus的编写方式，scoped表示在这个组件中编写的样式不会对其他组件产生影响。
   2. 设计稿是一个2倍尺寸的图片，所以量出来的header的高度是86px，但实际上是43px。
   3. 1rem = html font-size = 50px，在reset.css中font-size是50px， rem是相对于这个50px的尺寸，所以43px实际上对应的是.86rem，刚好是测量出来的那个像素数除以100所得到的值。  这些像素单位之间的关系，参考[这篇简书](https://www.jianshu.com/p/1b69f0df78f3)

## iconfont的使用
1. 下载流程： [iconfont](https://www.iconfont.cn) ---> 图标库 ---> 官方图标库 ---> 大麦官方图标库 ---> 将需要的图标放入购物车 ---> 添加至Travel项目中 --- > 下载至本地 
2. 解压后，将里面的字体文件.eot, .svg, .ttf, .woff放入在项目中的styles目录中创建的iconfont目录中，将iconfont.css文件放入styles目录下。
3. 修改iconfont.css文件中的对应iconfont目录下的四个文件的文件路径。base64的不用改。
4. 引入iconfont:
   ```javascript
   // 在项目的入口模块main.js中import
   import './assets/styles/iconfont.css'
   ```
5. 使用iconfont
从iconfont官网的已添加的项目中复制需要使用的字体图标的十六进制代码，放在Header组件中需要呈现的位置。

## 代码优化
1. 主色调优化： 
   - 优化目的：一个项目中的主色调，在很多地方都会用到，会出现大量的重复使用现象，---可以把这个颜色单独的放到一个变量中，需要使用的饿地方直接引用就可以，当网站的主色调需要改变的时候，只要改变这个变量的值，全局的主色调都会变化，可维护性将会得到比较大的提升。
   - 变量的创建： 在styles目录下创建variables.styl文件，在该文件中设置网站主色调`$bgColor = #00bcd4`
   - 主色调的使用： 比如在Header.vue中使用，首先要在`<style></style>`标签中引入样式，即`@import '../../../assets/styles/variables.styl'`，然后在需要设置该颜色的地方使用：`background: $bgColor`。
2. 路径优化：
    - 优化目的： 很多样式文件或者是组件文件的都需要使用styles目录下的内容，引用时路径中含有重复的部分，比如，在main.js中
        ```javascript
        import './assets/styles/reset.css'
        import './assets/styles/border.css'
        import './assets/styles/iconfont.css'
        ```
    考虑前面的重复路径部分能不能用一个特殊符号表示，就像是@表示的是src目录一样(注意：要想在css样式中引用其他的css样式使用@时要在前面添加~符号，即`@import '~@/assets/styles/variables.styl'`)。
    - 优化方法： 在build目录下的webpack.base.conf.js文件中设置重复路径对应的特殊符号， reslove中有一个叫alias的别名项，里面默认存在一个指向，即：`'@': reslove('src)`, 所以，我们自定义一个别名：`'styles': reslove('src/assets/styles)`，所以，原来重复的位置都可以替换成styles来表示，即
        main.js中
        ```javascript
            import 'styles/reset.css'
            import 'styles/border.css'
            import 'styles/iconfont.css'
        ```
        Header.vue中
        ```html
            <style>
                @import '~styles/variables.styl'
            </style>
        ```
