## 项目环境准备 
   1. [Node.js](https://nodejs.org/en/)
   2. [git](https://git-scm.com)
   3. 线上管理仓库：[码云](https://gitee.com)  --- 使用SSH公钥（码云上有教程， 用GitBash执行）
   4. [Vue命令行工具](https://cn.vuejs.org/v2/guide/installation.html)的安装：
      1. npm install --global vue-cli
      2. vue init webpack 项目名称  --- 注意： 根据向导在写项目名称的时候不能含有大写字母
      3. cd 项目名称
      4. npm install  ---因为此时只有package.json文件，没有node_modules目录
      5. npm run dev  --- 去浏览器运行返回的地址能出现一个带有vue绿色logo的页面，就说明配置成功
   5. 将本地文件push到线上仓库
      1. git add .
      2. git commit -m '项目描述'
      3. git push

## 项目主要文件结构
   1. package.json ---  第三方模块的依赖
   2. package-lock.json --- 锁定第三方依赖包的版本
   3. index.html --- 默认的首页模板文件
   4. static静态资源目录 --- 静态图片， json数据 
   5. src目录 --- 整个项目的源代码
      1. main.js --- 整个项目的入口文件
      2. App.vue --- 项目中最原始的那个根组件
      3. router/index.js --- 存放项目路由信息
      4. components --- 放的是项目中可能会用到的一些小组件
      5. assets --- 项目中会用到的一些图片资源，比如logo
   6. config文件夹 --- 项目的配置文件目录
   7. build目录 --- 打包的一些webpack的配置文件的内容，  是由vue-cli自动配置生成的，一般不需要修改 

## 单文件组件与路由
单文件组件： 简单来说就是使用单独的.vue文件。根组件中的 `<router-view />` 表示： 显示的是当前路由地址所对应的内容。
单文件组件内容组成：
```javascript
<template>
  <div id="app">
    <router-view/>
  </div>
</template>

<script>
export default {
  name: 'App'
}
</script>

<style>

</style>

```
路由： 客户端请求路径与服务端返回路径之间的对应关系。
注意： 在路由中使用新的组件的时候，不要忘记将该组件import进去

## 多页应用与单页应用
1. 多页面应用 ： 页面跳转 ---> 每次都要发出http请求，返回HTML
   优缺点： 首屏时间快，SEO效果好； 但是页面的切换慢
2. 单页面应用：  页面跳转 ---> JS渲染，节约了很多http请求时延
   优缺点: 页面切换快， 首屏时间稍慢， SEO差 
3.  有这样的缺点，为什么还要使用单页面应用的方式？
   vue中有相关的服务端渲染的技术，可以完美解决这些缺点。
4. 在这里页面之间的跳转不使用超链接的形式，而是使用`<router-link to="/link"></router-link>`.  
5. 注意： `<template></template>`模板里面向外暴露只能暴露一个根标签，把写的内容都放在一个div中就可以了，就不会出现层级的问题。

## 项目代码的初始化
- 添加网页的viewport的meta标签： --- 目的：用户使用手指改变页面大小的操作是无效的，页面的比例始终是1:1
    ```html
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
   ```
- 引入 reset.css （一个重置的样式表）
    在不同的手机浏览器上，默认的一些样式是不统一的，需要把这些手机的初始化的样式统一起来。
- 在项目中应用reset.css --- 在项目的入口文件main.js文件顶部import进去。
- 引入border.css:
   移动端存在1像素边框的问题， 有的手机的屏幕分辨率比较高，可能是2倍屏或者3倍屏， 在页面上写`border-bottom: 1px solid red;的时候，这里的1px指的是css像素，它在2倍屏上，实际上对应的不是一个物理像素的高度，而是2个或者3个物理像素的高度，为了解决在高倍屏里面1像素边框会被显示成多像素的问题，需要引入一个1像素边框的解决方案，即 border.css。
- 应用border.css ---  在项目的入口文件main.js文件顶部import进去。
- 解决移动端开发中的300毫秒点击延迟的问题：
    在项目根目录，npm一个fastclick第三方库：
    ```shell
    npm install fastclick --save
    ```
    安装完成后，在项目的入口文件main.js文件顶部import进去。然后把它绑定到`document.body`中：
    ```javascript
    fastClick.attach(document.body)
   ```
- 管理在项目中要使用到的icon：
  - [iconfont](https://www.iconfont.cn)
  - 注册账号(使用GitHub账号登录的)
  - 图标管理 ---> 我的项目 ---> 创建项目(Travel)
- 将修改后的代码更新的线上仓库(码云)
