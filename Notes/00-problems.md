### 这个文件用来记录在项目开发工程中遇到的问题以及解决方法

1. 在ajax获取数据的时候修改了项目的配置，修改完config/index.js中的proxyTable后重启服务器报错：
   报错信息是：
   ```shell
    ERROR  Failed to compile with 1 errors                                                                                              下午7:35:19

    This relative module was not found:

    * ../../../vue-temp/vue-editor-bridge in ./node_modules/babel-loader/lib!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./src/pages/home/Home.vue
   ```
   原因是： VScode编辑器增加了一行代码`import func from '../../../vue-temp/vue-editor-bridge';` ,直接把这个import项删除掉重启服务器即可。

2. 