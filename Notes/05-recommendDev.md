## 目标： 实现热销推荐与周末游组件的开发

与之前一样：在码云上创建分支，拉到本地，切换到该分支进行相应组件的开发。

### 热门推荐组件的开发
1. 准备工作：Recommend.vue组件的创建，基本框架搭建与引入操作。
2. 热门推荐字样的布局与样式设置：
   1. 布局
    ```html
        <div class="title">热销推荐</div>
    ```
    2. 样式
    ```html
     css样式
        .title
        margin-top .2rem
        line-height .8rem
        background-color #eee
        text-indent .2rem
    ```
3. 使用li来实现
   1. 布局
   ```html
   <ul>
        <li class="item border-bottom">
            <img class="item-img" src="http://img1.qunarzz.com/sight/p0/1707/82/821428e3bbf93bbaa3.water.jpg_200x200_1f657041.jpg" >
            <div class="item-info">
                <p class="item-title">迪士尼小镇</p>
                <p class="item-desc">上海市浦东新区迪士尼小镇申迪西路255弄</p>
                <button class="item-button">查看详情</button>
            </div>
        </li>
    </ul>
   ```
   li标签上的border-bottom类表示的是让li的下方有一个1px的边框(写上这个类名就可以实现1px边框的设置，因为在项目一开始的时候，我们已经对这个问题进行了设置)。
    2. 样式
   ```html
    .item
        overflow hidden
        display flex
        height 1.9rem
        .item-img
            width 1.7rem
            height 1.7rem
            padding .1rem
            .item-info
                flex 1
                padding .1rem
                .item-title
                    line-height .54rem
                    font-size .32rem
                    ellipsis()
                .item-desc
                    line-height .4rem
                    color #ccc
                    ellipsis()
                .item-button
                    line-height .44rem
                    margin-top .16rem 
                    background-color #ff9300
                    padding 0 .2rem
                    border-radius .06rem
                    color #fff
   ```
   解决文字溢出问题，直接在可能会溢出的地方使用方法ellipsis()方法，因为之前已经对这个方法进行了封装。但是，在使用之前要记得引入，即在style标签顶部`@import '~styles/mixins.styl'`。此时，会发现文字比较多的时候还是没有出现省略号的提示，而是一种像是直接将溢出的文字截断的效果。解决方法： 在.item-info中添加一个属性：`min-width 0`。

4. 定义data函数对代码进行优化， 利用循环的形式生成多个li。与前面操作一致，不再重复。

### 周末游组件开发
1. 准备工作：Weekend.vue组件的创建，基本框架搭建与引入操作。
2. 会发现，该组件的布局与Recommend组件基本一致，可以将代码复制过来直接在上面进行修改。
   1. 布局
   ```html
    <ul>
        <li class="item border-bottom" 
            v-for="item of recommendList" 
            :key="item.id"
        >
            <div class="item-img-wrapper">
                <img class="item-img" :src="item.imgUrl" >
            </div>
            <div class="item-info">
                <p class="item-title">{{item.title}}</p>
                <p class="item-desc">{{item.desc}}</p>
            </div>
        </li>
    </ul>
   ```
    2. 样式
    ```html
    <style lang="stylus" scoped>
    @import '~styles/mixins.styl'
        .title
            margin-top .2rem
            line-height .8rem
            background-color #eee
            text-indent .2rem
        .item-img-wrapper
            overflow hidden
            height 0
            padding-bottom 37.09%   图片的宽高比
            .item-img
                width 100%  自适应大小
        .item-info
            padding .1rem
            .item-title
                line-height .54rem
                font-size .32rem
                ellipsis()
            .item-desc
                line-height .4rem
                color #ccc
                ellipsis()
    </style>
    ```

一个思考问题：  在Swiper组件中我们使用的是一个swiper第三方组件来完成的，我们往这个组件中传递了很多的内容，这些内容是通过什么样的形式传递上去的？---slot插槽的形式，将页面中的具体数据传递给了swiper这个第三方组件。slot到底是干什么用的？ 有时，在用组件的时候希望一部分内容可以被父组件定置的时候，会使用slot的形式往这个组件中传递一些可以被自己定置的内容。





