## 目标： 城市选择页面的路由配置

### 1. 准备工作： 创建一个city-router分支，准备城市页面路由的配置
### 2. 城市页面的创建：
首先，在src/pages下创建一个city目录，在city目录中创建City.vue作为城市页面的主页。
然后，在src/router/index.js文件中开创一个名为City的新页面：
```javascript
routes: [ // 当用户访问根路径的时候， 给用户展示的是HelloWorld这个组件
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/city',
      name: 'City',
      component: City
    }
  ]
```
注意：不要忘记import这个页面，即在这个index.js的顶部`import City from '@/pages/city/City'`

### 3. 实现页面跳转
1. 首先为City.vue创建基本框架
   ```html
   <template>
    
    </template>

    <script>
    export default {
        name: 'City'
    }
    </script>

    <style lang="stylus" scoped>
        
    </style>
   ```
2. 当点击header中的header-right区域的时候希望能够实现页面的跳转，使用router-link，这个是能够直接实现页面跳转的内置组件：
   ```html
    <router-link to="/city">
        <div class="header-right">
            {{this.city}}
            <span class="iconfont arrow-icon">&#xe843;</span>
        </div>
    </router-link> 
   ```
   因为/city这个路径已经在路由中做了配置，所以点击的时候会直接跳转到City.vue页面。但是会发现，header-right区域的文字会变成`#25a4bb`这个颜色，这是因为，在header-right外层添加router-link相当于加的是一个a标签。 我们可以给header-right设置一个自己的文字颜色，即添加`color: #fff`即可。
   
### 4. 城市列表页面的开发
4.1 准备： 
在city文件夹下创建一个components目录，在components目录中创建Header.vue子组件，然后进行基本框架搭建，取名为CityHeader。在城市主页中引入这个组件（过程与Home中的一致，不再详细展开）。
4.2 样式设置：
```html
.header
    overflow hidden
    height .86rem
    line-height .86rem
    text-align center
    color #fff
    background $bgColor  // 使用这个颜色变量不要忘记引入variables.styl文件
    font-size .32rem
```
4.3 返回箭头的制作
借用home中的返回箭头
```html
<div class="iconfont header-back">&#xe624;</div>
```
样式设置：
```html
.header-back
    position absolute
    top 0
    left 0
    width .64rem
    text-align center
    font-size .4rem
```
实现箭头的返回首页功能: 要这样写的话，要记得给箭头加一个`color #fff`
```html
<router-link to="/">
    <div class="iconfont header-back">&#xe624;</div>
</router-link>
```
4.4 代码小优化
比如：在header中height与line-height都是.86rem，就可以在variable是.styl定义一个$headerHeight = .86rem, 就可以在home的Header.vue和city的Header.vue中使用这个高度变量了，以后如果想要做出调整，只要修改这个变量的值就可以全部修改。

### 5. 代码提交
