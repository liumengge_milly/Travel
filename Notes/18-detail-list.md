## 目标：使用递归组件实现详情页列表的开发

1. 准备阶段： 创建分支detail-list，创建组件List.vue，组件引入。
2. 第一层列表的实现：
   
基本布局是：
   
```html
<div class="item">
    <div class="item-title border-bottom">
        <span class="item-title-icon"></span>
        成人票
    </div>
    <div class="item-title border-bottom">
        <span class="item-title-icon"></span>
        儿童票
    </div>
    <div class="item-title border-bottom">
        <span class="item-title-icon"></span>
        学生票
    </div>
    <div class="item-title border-bottom">
        <span class="item-title-icon"></span>
        特惠票
    </div>
</div>
```

样式：
```
.item-title
    line-height .8rem
    font-size .32rem
    padding 0 .2rem
    .item-title-icon
        display inline-block
        position relative
        left .06rem
        top .06rem
        width .36rem
        height .36rem
        background url(http://s.qunarzz.com/piao/image/touch/sight/detail.png) 0 -.9rem no-repeat
        margin-right .1rem
        background-size .4rem 3rem
```

从父组件Detail.vue中传递过来的数据是这样的：
```javascript
data() {
    return {
        list: [{
            title: '成人票'
        }, {
            title: '儿童票'
        }, {
            title: '学生票'
        }, {
            title: '特惠票'
        }]
    }
}
```

就可以使用循环的形式实现：
```html
<div 
    class="item" 
    v-for="(item, index) of list"
    :key="index"
>
    <div class="item-title border-bottom">
        <span class="item-title-icon"></span>
        {{ item.title }}
    </div>
</div>
```

在详情页的这个列表中他还会存在第二层列表，此时数据是这样的：
```javascript
data() {
        return {
            list: [{
                title: '成人票',
                children: [{
                    title: '成人三人团票',
                }, {
                    title: '成人五人团票'
                }]
            }, {
                title: '儿童票'
            }, {
                title: '学生票'
            }, {
                title: '特惠票'
            }]
        }
    }
```

这时想要让这个第二层列表呈现出来就要借助递归，递归就是组件的自身调用自己这个组件，即：
```html
<div 
    class="item" 
    v-for="(item, index) of list"
    :key="index"
>
    <div class="item-title border-bottom">
        <span class="item-title-icon"></span>
        {{ item.title }}
    </div>
    <div v-if="item.children" class="item-children">
        <detail-list :list="item.children"></detail-list>
    </div>
</div>
```
v-if加一个判断，如果数据中有children这一项，就说明它应该是一个多级的菜单。

在每个组件中都有一个名字，这个名字在调用递归组件的时候起到了很大的作用，当一个组件需要使用自己的时候，就通过自己的名字来使用。上述代码表示，如果数据中有children，就把children再次传给list，实现一个递归的循环。这时页面上二级的分类也会被显示出来，稍微增加一点样式:
```html
.item-children
    padding 0 .2rem
```

当我还有三级菜单的时候，即我的数据是这样的：
```javascript
data() {
    return {
        list: [{
            title: '成人票',
            children: [{
                title: '成人三人团票',
                children: [{
                    title: '成人三人团票-套餐一'
                }, {
                    title: '成人三人团票-套餐二'
                }]
            }, {
                title: '成人五人团票' 
            }]
        }, {
            title: '儿童票'
        }, {
            title: '学生票'
        }, {
            title: '特惠票'
        }]
    }
}
```
页面上同样会呈现出三级列表。


3. 小bug： 当页面往下拖动的时候，header-fixed不会把列表项盖住，我们想要的是将列表项遮盖的效果，给它加一个z-index即可。

#### 做个标记： 这一部分的主要目的是学习递归组件的使用，列表的具体内容以后再继续完成！！！

