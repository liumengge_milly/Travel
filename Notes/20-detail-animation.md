## 目标： 在项目中添加一个基础动画效果

当点击详情页的图片轮播组件时，希望它能够显示出渐隐渐现的效果。

1. 创建一个公共的渐隐渐现的组件`src/common/fade/FadeAnimation.vue`，并搭建基础框架。
2. 单个组件的动画写法：

```html
<transition>
    <slot></slot>
</transition>
```
其中slot是外部组件插入进来的一个插槽。如果想让transition有一些动画效果，vue会在一些时间点上给transition标签插入一些class，借助这些class就可以实现动画效果了。

```html
.v-enter, .v-leave-to
    opacity 0
.v-enter-active, .v-leave-active
    transition opacity 0.5s
```
至此，这个动画组件就完成了。

3. 动画组件的使用
   
首先将这个动画组件FadeAnimation.vue引入Banner.vue组件中并进行注册，接下来就可以直接使用了：

```html
<fade-animation>
    <common-gallery 
        :imgs="bannerImgs" 
        v-show="showGallery"
        @close="handleBannerGallery"
    ></common-gallery>
</fade-animation>
```
这样的话common-gallery就会作为一个插槽，插入到FadeAnimation.vue组件中，FadeAnimation.vue中的slot代表的就是common-gallery，在动画组件FadeAnimation.vue中相当于是在common-gallery外部添加了一个动画效果，所以common-gallery进行展示或者隐藏的时候就会出现一个渐隐渐现的效果。
