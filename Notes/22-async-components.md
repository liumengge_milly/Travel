## 目标：学习Vue中异步组件的使用

首先看，webpack打包生成的dist目录中包含的内容：有一个index.html文件，这个就是前端代码的入口的html文件；有一个static目录。里面会有一个css文件夹，css.map文件目的是帮助我们调试被压缩过的css代码，方便在开发的时候调试的使用，真正在线上有用的文件是这个css文件，里面是我们所有页面要用到的css，还有个js目录，有用的有3个文件：app.js、manifest.js、vendor.js，manifest.js文件可以理解成webpack打包生成的一个配置文件，vendor.js中存放的是各个页面各个组件公用的一些代码，app.js里面是项目的各个页面的业务逻辑代码。

在浏览器上请求`localhost/vueProject01`的时候，Network中的JS会加载上面说的这3个JS文件，异步组件主要是app.js的内容。因为app.js是包括所有页面的逻辑代码，但是当我访问首页的时候是不需要加载其他页面的代码的，但是这种默认的打包方式会使得在请求一个页面的时候把所有页面的内容全部加载，性能比较低。这个项目中app.js是比较小的，但是当项目越来越大的时候，打包之后的app.js有可能会达到1MB以上，这时就需要使用异步组件进行一个优化。

当我们访问前端的服务器`localhost:8081`的时候，会发现访问首页的时候Network下只加载了app.js，也是把所有内容都加载好了，访问其他页面的时候不会再有新的内容加载。什么叫使用异步组件加载？ 就是我访问首页就只加载首页，访问详情页只加载详情页，需要什么加载什么而不是一次性的加载。

`router/index.js`中，之前都是直接引入对应的组件，然后直接使用这个组件的：
```javascript
import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/home/Home'
import City from '@/pages/city/City'
import Detail from '@/pages/detail/Detail'


Vue.use(Router)

export default new Router({
  routes: [ // 当用户访问根路径的时候， 给用户展示的是HelloWorld这个组件
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/city',
      name: 'City',
      component: City
    }, {
      path: '/detail/:id',
      name: 'Detail',
      component: Detail
    }],
    scrollBehavior (to, from, savedPosition) {
      return {
        x: 0,
        y: 0
      }
    }
})
```

下面改成使用异步组件的方式：
```javascript
import Vue from 'vue'
import Router from 'vue-router'


Vue.use(Router)

export default new Router({
  routes: [ // 当用户访问根路径的时候， 给用户展示的是HelloWorld这个组件
    {
      path: '/',
      name: 'Home',
      component: () => import('@/pages/home/Home')
    },
    {
      path: '/city',
      name: 'City',
      component: () => import('@/pages/city/City')
    }, {
      path: '/detail/:id',
      name: 'Detail',
      component: () => import('@/pages/detail/Detail')
    }],
    scrollBehavior (to, from, savedPosition) {
      return {
        x: 0,
        y: 0
      }
    }
})
```

但是，当我们的项目比较小的时候是不提倡使用异步组件的，因为，虽然请求首页的时候只加载了首页的逻辑，加载的内容少了，但是当请求城市选择页面的时候会额外的发起一次http请求去加载城市选择页面的逻辑，当一个app.js很小的时候，发一个http请求的代价远比首页多加载一点js代码的代价要高。在app.js比较大的时候才建议使用。 

如果是使用异步组件的话，不只有在router里面可以这样用，在Home.vue中也可以异步加载Header.vue等子组件，方式是一样的。