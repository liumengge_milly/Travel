## 目标： Vue项目的接口联调，真机测试与webpack打包上线

### 1. Vue项目的接口联调

之前我们在写代码的时候，使用的ajax数据都不是从后端返回的真实数据，而是我们自己通过接口mock模拟的假数据。当前端的代码已经编写完毕，后端的接口已经写好的时候，就需要把前端模拟的数据去掉，去尝试使用后端提供的数据，进行前后端的一个调试，这个过程就是前后端的联调。

将mock当中的数据切换成后端提供的真实数据。 在开启了一个PHP的条件下，希望它请求的是在这个服务器中返回的数据。此时，将static目录下的mock删除之后会发现，按照原来的请求路劲请求页面时会有404的错误，数据找不到了，接下来就需要进行配置。

在项目中的`config/index.js`有一个proxyTable，原本的配置是这样的：
```javascript
proxyTable: {
    '/api': {
    target: 'http://localhost:8081',
    pathRewrite: {
        '^/api': '/static/mock'
    }
    }
}
```
它表示的是用户访问`/api`这个路径，会帮助你把这个请求接到`localhost:8081`这个端口上，这个端口是我们前端服务器的一个端口，现在我们想要把它打到后台服务器的端口上，（此时我电脑上的WampSever是打开的状态)，我后端服务器也是`localhost`但是它的端口号是默认的80端口，这个http的80端口可以省略不写。所以一旦请求`/api`就会把这个请求转发到`http://localhost:80`这个端口上，也就是PHP服务器上，它会做一个url的rewrite，会把以`/api`开头的url映射到PHP服务器的`/static/mock`目录下:

```javascript
proxyTable: {
    '/api': {
    target: 'http://localhost',
    pathRewrite: {
          '^/api': '/static/mock'
        }
    }
}
```
改变配置文件的时候记得重启服务器。

在这里，前后端都在本地跑的，但是实际的开发中有可能后端并不在本地，而是在后端开发人员自己的电脑上，或者是一个内网或者外网的服务器上，这种情况下，这个proxyTable中`localhost`就要写内网的IP地址或者外网的域名。通过这种形式，就可以把前端向`/api`这个地址的请求代理转发给任何一台后台服务器，从而实现前后端的联调。所以，就不需要再使用像fiddler，charles这样的抓包代理工具了，只需要使用proxyTable根据根据服务器地址进行配置就可以了。

注： 本次联调没有成功？？？我把原本代码中的模拟数据的整个mock文件夹都删除了，为什么还是能够请求到数据？？？修改上述的target值也还是能访问到？？？把后端的服务器关掉了也还是能访问到数据？why？！

换了个浏览器就行了，， 明明把浏览器的缓存也都删掉了，这里还需要继续寻找答案。


### 2. Vue项目的真机测试

获取本机IP地址： cmd 中输入 ipconfig.

当将localhost换成本地地址的时候会发现拒绝了我们的连接请求。但是在访问后台服务器的80端口时是正常的。(我在这里出现了问题，是Apache配置的问题，遇到的问题以及相关配置解决方法已经整理到了我的CSDN上的PHP目录下)。

上述现象说明，我们的ip地址没有问题，只不过8081端口不能被外界访问，原因是前端的项目是通过`webpack-dev-sever`启动的,`webpack-dev-sever`默认不通过ip的形式进行页面的访问，所以需要将它默认的配置项做一个修改。


```javascript
"scripts": {
    "dev": "webpack-dev-server --inline --progress --config build/webpack.dev.conf.js",
    "start": "npm run dev",
    "lint": "eslint --ext .js,.vue src",
    "build": "node build/build.js"
  }
```
在我们项目的package.json文件中可以发现，当我们每次`npm run start`启动服务器的时候，实际上都在运行`webpack-dev-server --inline --progress --config build/webpack.dev.conf.js`,也就是帮我们启动了一个`webpack-dev-sever`,如果想让webpack-dev-sever能够通过ip地址被访问的话，在其后面加一个配置项即可：
```javascript
"scripts": {
    "dev": "webpack-dev-server --host 0.0.0.0 --inline --progress --config build/webpack.dev.conf.js",
    "start": "npm run dev",
    "lint": "eslint --ext .js,.vue src",
    "build": "node build/build.js"
  }
```
现在使用ip地址的形式来访问就没有问题了。这样，就可以使用手机在同一个局域网内访问这个网页了。
 
在手机上的测试会有一个bug：在城市列表页拖动右侧的字母列的时候，整个屏幕会跟着上下滚动。

解决：`pages/city/components/Alphabet`中在touchstart的时候添加一个事件修饰符，它可以阻止touchstart的默认行为，即：
```javascript
<ul>
    <li class="item" 
        v-for="item of letters" 
        :key="item"
        :ref="item"
        @click="handleLetterClick"
        @touchstart.prevent="handleTouchStart"
        @touchmove="handleTouchMove"
        @touchend="handleTouchEnd"
    >
        {{ item }}</li>
</ul>
```

在真机测试时，手机可能会出现白屏的效果，可能原因是手机浏览器上默认不支持Promise，解决这个问题需要安装一个第三方的包：`npm install babel-polyfill --save`,这个包会帮助没有Promise的浏览器自动添加ES6的新特性。然后进入到main.js入口文件中去`import 'babel-polyfill'`。如果还是白屏的情况，那可能不是代码的问题，而是webpack-dev-server的问题，使用webpack打包上线后就不会有问题了。


在真机测试的时候发现一个小bug：我在城市选择页面的搜索框中想要搜索"北京"，但是我用的是英文字母，当输入'bei'的时候，北京就被搜索出来了，我点击搜索出来的北京之后页面就回到了首页上，但是当我再次回到城市列表页的时候会发现，刚才输入的'bei'还在搜索框内，搜索出来的结果也还在那里，我希望当我再次回来的时候搜索框内是没有内容的，也就是相当于这个页面应该是刷新过的：解决方法，在Search.vue中当我点击搜索出来的'北京'列表后，页面要要跳转到首页上，这时可以立即将keyword清空。一开始我是想让`this.list=[]`的，但是发现没有效果，这是因为之前代码有写，this.list是不是空数组取决于是不是有搜索关键字keyword，当只执行`this.list=[]`时，keyword并没有清空，导致这个if语句不会被执行，所以直接设置`this.keyword=''`才是正确的解法。

keyword的侦听器：
```javascript
watch: {
    keyword () {
        if (this.timer) {
            clearTimeout(this.timer)
        }
        if (!this.keyword) { // 
            this.list = []  
            return
        }
        this.timer = setInterval(() => {
            const result = []
            for (let i in this.cities) {
                this.cities[i].forEach((value) => {
                    if (value.spell.indexOf(this.keyword) > -1 || value.name.indexOf(this.keyword) > -1) {
                        result.push(value)
                    }
                });
            }
            this.list = result
        }, 100);
    }
}
```

修改：
```javascript
handleCityClick (city) {
    this.changeCity(city)
    this.$router.push('/')
    this.keyword = ''
}
```

### 3. webpack打包上线

1. 在项目中打开终端执行：`npm run build`，这时vue的脚手架工具会帮我们自动的对src目录下的源代码进行打包编译，生成一个能被浏览器运行的代码，这个代码也是一个压缩后的代码，打包完成后控制台会有`Build complete`提示。此时会发现，我们的项目中多出来一个叫`dist`的目录，这个目录里面的代码就是我们最终要上线的代码。

2. 接下来就是将dist这个目录给后端开发人员，他们会把这个代码放到后端服务器上。

3. 这里，我在自己的电脑上可以打开Apache服务器，找到服务器的根路径，有一个`htdocs`，这里就是后台服务器的根路径。把刚才的dist目录下的两个文件放到这个`htdocs`目录下就可以了。这时候我们在浏览器上访问这个页面的时候就可以直接访问`localhost`而不是`localhost:8081`。我们访问的这个localhost实际上是后端的服务器，后端的服务器已经有了前端的代码，所以它会把默认的`index.html`文件显示出来，同时在`index.html`文件上又引入了我们打包生成的css和js文件，前端的代码就可以在后端的服务器上运行起来。后端服务器上还有后端提供的接口，这样就把前端代码融合到了后端的项目中，整个把后端项目进行上线，网站也就完成了。

4. 一个简单的vue项目打包上线流程： `npm run build` ---> dist ---> 给后端开发人员 ---> 放到后端项目的根目录

5. 如果我不想将dist目录中的文件直接放在后端服务器的根目录下，而是想将它们放在一个名为`vueProject01`目录下，而在网页上进行访问的时候通过`localhost/vueProject01`来访问的时候，会出现访问路径错误的问题，如果我们希望打包生成的文件最终放在后端的一个`vueProject01`目录下进行运行的话，需要对前端代码进行修改。

`config/index.js`中有一个build打包部分的配置项，把`assetsPublicPath: '/'`替换成`assetsPublicPath: '/vueProject01'`，也就是表示我打包的这个项目要运行在后端的`vueProject01`这个目录下。

修改之后重新打包，重复之前的操作。
