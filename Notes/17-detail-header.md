## 目标： 实现详情页中header渐隐渐现效果

当进入详情页的时候，详情页的左上角有一个返回的按钮，向下拉详情页的时候，返回箭头会慢慢消失，取而代之的是一个蓝色的header。

1. 创建Header.vue组件，完成基本的框架构建与引入操作
2. 返回箭头的基本布局与样式美化：

```html
<div>
    <div class="header-abs">
        <div class="iconfont header-abs-back">&#xe624;</div>
    </div>
    <div class="header-fixed"></div>
</div>
```
```
.header-abs
    position absolute
    top .2rem
    left .2rem
    width .8rem
    height .8rem
    line-height .8rem
    border-radius .4rem
    text-align center
    background rgba(0, 0, 0, .6rem)
    .header-abs-back
        font-size .4rem
        color #fff
```

当点击这个返回箭头的时候希望它跳转回主页：
```html
<div>
    <router-link tag="div" to="/" class="header-abs">
        <div class="iconfont header-abs-back">&#xe624;</div>
    </router-link>
    <div class="header-fixed"></div>
</div>
```

3. header-fixed部分的布局与样式美化。 这个与城市选择页面的header几乎一样，复制过来稍微改动即可：
   
```html
<div class="header-fixed">
    <router-link to="/">
        <div class="iconfont header-fixed-back">&#xe624;</div>
    </router-link>
    景点详情
</div>
```
```
.header-fixed
    position fixed
    left 0
    right 0
    top 0
    height $headerHeight
    line-height $headerHeight
    text-align center
    color #fff
    background $bgColor
    font-size .32rem
    .header-fixed-back
        position absolute
        top 0
        left 0
        width .64rem
        text-align center
        font-size .4rem
        color #fff
```

4. 逻辑实现： 当进入这个详情页面的时候，返回箭头是默认显示的，而header-fixed部分则与之相反， 页面向下滑动大概60px的时候返回箭头渐渐隐藏，header-fixed逐渐显示出来。所以我们就需要对`window.addEventListener`事件进行监听。

因为我们使用了keep-live，所以页面一被展示，activated这个生命周期钩子就会被执行。
```javascript
methods: {
    handleScroll () {
        console.log(document.documentElement.scrollTop)
    }
},
activated () {
    window.addEventListener('scroll', this.handleScroll)
}   
```

根据上面的逻辑，我们在获得向上的滚动距离后就可以这样做:
```javascript
methods: {
    handleScroll () {
        const top = document.documentElement.scrollTop
        if (top > 60) {
            this.showAbs = false
        }else {
            this.showAbs = true
        } 
        
    }
}
```

基本的逻辑实现了，接下来就是渐隐渐现的效果。首先，给header-fixed设置一个动态的style，并在data中定义这个opacityStyle数据，默认情况下它的opacity为0.希望的是，当用户的滚动距离在60px到140px之间会出现在这个渐隐渐现的效果。

```html
<div 
    class="header-fixed" 
    v-show="!showAbs"
    :style="opacityStyle"
>
    <router-link to="/">
        <div class="iconfont header-fixed-back">&#xe624;</div>
    </router-link>
    景点详情
</div>
```

```javascript
data() {
    return {
        showAbs: true,
        opacityStyle: {
            opacity: 0
        } 
    }
},
methods: {
    handleScroll () {
        const top = document.documentElement.scrollTop
        if (top > 60) {
            let opacity = top / 140
            opacity = opacity > 1 ? 1 : opacity
            this.opacityStyle = {
                opacity: opacity
            }
            this.showAbs = false
        }else {
            this.showAbs = true
        } 
        
    }
}
```
其中的三元表达式的意思是，当滚动的距离大于140px的时候header-fixed的opacity一直是1。上面代码中设置opacityStyle的部分使用ES6的写法：
```javascript
methods: {
    handleScroll () {
        const top = document.documentElement.scrollTop
        if (top > 60) {
            let opacity = top / 140
            opacity = opacity > 1 ? 1 : opacity
            this.opacityStyle = { opacity }
            this.showAbs = false
        }else {
            this.showAbs = true
        } 
        
    }
}
```

至此，渐隐渐现的替换效果就实现了。

### 代码提交

补充： 对全局事件的解绑

问题： 在详情页面加载的时候，我们使用了生命周期钩子activated，在这里对window做了一个事件的绑定。我们在一个组件的某一个标签上绑定一个事件不会有什么问题，它不会影响到其他的组件，但是当我们给一个全局对象绑定一个事件的时候，将会对外部的组件也会产生影响。

我们在handleScroll方法中输出一个`console.log(scroll)`，在详情页面滚动的时候，scroll会被不断的输出，当返回到首页滚动页面的时候，scroll依然会被输出，所以这里看出，把事件绑定在全局对象上将会对其他组件也会产生影响。

我们使用了keep-alive的时候，会多出一个activated的生命周期函数，它在每次页面展示的时候会被执行，与之对应的，还提供了了一个叫做deactivated的生命周期函数，它将在页面即将被隐藏或者是即将被新的页面替换的时候执行，我们就可以利用这个生命周期钩子在页面离开这个页面之间解绑全局对象上绑定的事件：

```javascript
deactivated () {
    window.removeEventListener('scroll', this.handleScroll)
}
```

### 代码提交
这次代码修改的时候没有创建分支，直接修改了文件。先在master分支push上去，然后切换到上次的detail-header分支，然后和master进行一个合并再push上去。

1. git branch 可以查看分支
2. git add . 
3. git commit -m 'fix problem'
4. git push
5. git checkout detail-header
6. git merge master
7. git push





