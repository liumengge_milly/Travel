## 目标： 实现城市选择页面的列表布局

### 1. 准备阶段： List组件的创建与引入
### 2. 城市表格的布局
2.1 首先会发现，整张列表当中可以分成“您的位置”，“热门城市”和“字母城市列表”三个部分，呈现三个区域：
```html
<div class="list">
    // 
    <div class="area">
        <div class="title border-topbottom">您的位置</div>
    </div>

    //
    <div class="area">
        <div class="title border-topbottom">热门城市</div>
    </div>

    //
    <div class="area">
        <div class="title border-topbottom">A</div>
    </div>
</div>
```
样式美化：
```
.border-topbottom   /* 使用伪元素设置边框*/
    &:before
        border-color #ccc
    &:after
        border-color #ccc
.title 
    line-height .54rem
    background #eee
    padding-left .2rem
    color #666
    font-size .26rem
```
2.2 “您的位置”区域
布局：
```html
<div class="area">
    <div class="title border-topbottom">您的位置</div>
    <div class="button-list">
        <div class="button-wrapper">
            <div class="button">上海</div>
        </div>
    </div>
</div>
```
样式美化：
```
.button-list
    padding .1rem .6rem .1rem .1rem  /*右侧留出alphabet的位置*/
    overflow hidden  /*触发BFC*/
    .button-wrapper
        float left
        width 33.3%
        .button
            text-align center
            margin .1rem
            padding .1rem 0
            border .02rem solid #ccc
            border-radius .06rem
```

2.3 “热门城市”区域
该区域与上述区域是一样的，只不过多加了一些显示的城市
2.4 “字母城市列表”区域
布局：
```html
<div class="area">
    <div class="title border-topbottom">A</div>
    <div class="item-list">
        <div class="item border-bottom">阿拉尔</div>
        <div class="item border-bottom">阿拉尔</div>
        <div class="item border-bottom">阿拉尔</div>
        <div class="item border-bottom">阿拉尔</div>
        <div class="item border-bottom">阿拉尔</div>
        <div class="item border-bottom">阿拉尔</div>
        <div class="item border-bottom">阿拉尔</div>
    </div>
</div>
```
样式美化：
```
.item-list
    .item
        line-height .76rem
        padding-left .2rem
```
添加下边线：
```
.border-bottom  /*这里的样式与border-topbottom是同一级别的*/
    &:before
        border-color #ccc
```
此时，三个区域基本布局完成，会发现，内容比较多的时候，因为没有添加`overflow: hdden`样式，所以多出的部分内容可以撑出去，页面时可以上下滚动的。我们想要取消这个效果，就可以给外层的list添加样式：
```html
.list
    overflow hidden
    position absolute
    top 1.58rem 
    left 0
    right 0
    bottom 0
    background-color pink
``` 
会发现，粉色的部分就是当前页面呈现的内容，此时页面超出的部分就超出了，页面也不能往下滚动，但是只有当前屏幕大小的话呈现的内容太少了，怎么办？ 我们选择使用插件， 使其呈现原生APP的拖拽效果，呈现效果更佳。

### 3. Better-scroll插件的使用
3.1 安装: `npm install better-scroll --save`
3.1 构造相同样式的布局：
```html
<div class="wrapper">
  <ul class="content">
    <li>...</li>
    <li>...</li>
    ...
  </ul>
  <!-- you can put some other DOMs here, it won't affect the scrolling-->
</div>
```
代码修改：在原来的`<div class="list"></div>`内部再包裹一层div。
3.2 应用：
官网给出的示例如下：
```javascript
import BScroll from '@better-scroll/core'
let wrapper = document.querySelector('.wrapper')
let scroll = new BScroll(wrapper)
```
上述示例在使用的时候需要操作DOM元素并创建实例， ref属性可以帮助我们获取DOM元素，所以我们可以给最外层的div添加一个ref属性，在`<script></script>`中`import BScroll from 'better-scroll'` 并创建一个生命周期函数(它会在页面DOM挂载完之后执行)：
```javascript
<script>
import Bscroll from 'better-scroll'
export default {
    name: 'CityList',
    mounted () {
        this.scroll = new Bscroll(this.$refs.wrapper)
    }

}
</script>
```
此时的页面便可以实现上拉和下拉的效果了，还带有一些动态效果。
关于ref可以参考[这里](https://blog.csdn.net/wh710107079/article/details/88243638?depth_1-utm_source=distribute.pc_relevant.none-task&utm_source=distribute.pc_relevant.none-task)和[这里](https://blog.csdn.net/weixin_41646716/article/details/80455506)

### 4. 右侧字母的布局
开创一个单独的组件Alphabet.vue, 并做好组件开发的基本模板构建与组件导入工作。
布局： 这里的数据都将会用ajax来获取，这里先这样表示。
```html
<div class="list">
    <ul>
        <li class="item">A</li>
        <li class="item">B</li>
        <li class="item">C</li>
        <li class="item">D</li>
        <li class="item">E</li>
        <li class="item">F</li>
        <li class="item">G</li>
        <li class="item">H</li>
    </ul>
</div>
```
样式美化：
```html
.list
    position absolute
    top 1.58rem
    right 0
    bottom 0
    width .4rem
```
实现里面的内容垂直居中显示：
```html
.list
    position absolute
    top 1.58rem
    right 0
    bottom 0
    width .4rem
    display flex
    flex-direction column
    justify-content center
```
实现内容的水平居中：
```
.item
    line-height .4rem
    text-align center
    color $bgColor
```
该页面的整体布局完成。

### 5. 代码提交
